from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
#from serials.models import Serial


class Profile(AbstractUser):
    avatar = models.ImageField(upload_to='avatar/', blank=True, null=True)
    profile_name = models.CharField("Имя", max_length=50)


# class Group(models.Model):
#     profile = models.ManyToManyField(Profile, blank=True, null=True)
#     serial = models.ForeignKey(Serial, on_delete=models.CASCADE)


