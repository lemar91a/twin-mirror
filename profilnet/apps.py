from django.apps import AppConfig


class ProfilnetConfig(AppConfig):
    name = 'profilnet'
