from django.shortcuts import render
from profilnet.models import Profile
from serials.models import Serial
from django.views.generic.edit import CreateView, UpdateView
from django.conf import settings
from django.urls import reverse

def landing(request):
    return render(request, 'landing.html')


def profile(request):
    profiles = Profile.objects.get(pk=request.user.id)
    serials = Serial.objects.all()
    return render(request, 'profilnet/profile.html', context={"profiles": profiles, "serials": serials})


class ProfileInfoUpdateView(UpdateView):
    model = Profile
    template_name = 'profilnet/profile_info_update.html'
    fields = ['avatar', 'profile_name']

    def get_success_url(self):
        return reverse('profile')


