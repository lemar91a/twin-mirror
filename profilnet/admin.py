from django.contrib import admin
from profilnet.models import Profile



class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'profile_name')


class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'profile', 'serial')


admin.site.register(Profile)
#admin.site.register(Group)