from django.urls import  path
from django.conf import settings
from django.conf.urls.static import static
from profilnet.views import ProfileInfoUpdateView

from profilnet import views


urlpatterns = [
    path('', views.profile, name='profile'),
    path('profile_info_update/<int:pk>', ProfileInfoUpdateView.as_view(), name='profile_info_update_url'),

]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)