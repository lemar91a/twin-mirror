from django.shortcuts import render
from django.views.generic import ListView, FormView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.db import IntegrityError
from django.http import HttpResponse
from django.shortcuts import redirect
from serials.models import Serial, FavoriteSerial, Rating, Season
from profilnet.models import Profile
from django.views.generic.base import View
from .forms import RatingForm
from django.db.models import Avg
#from django.template.defaultfilters import slugify
from pytils.translit import slugify


class ListSerialView(ListView):
      model = Serial
      template_name = 'serials/serial_list.html'
      queryset = Serial.objects.filter(published=True)


class DetailSerialView(DetailView):
    model = Serial
    template_name = 'serials/serial_detail.html'


    def get_context_data(self, **kwargs):
        try:
            context = super().get_context_data(**kwargs)
            context["form"] = RatingForm(initial={'star': Rating.objects.get(profile=self.request.user, serial=self.object).star_id})
            context['raiting'] = Rating.objects.filter(serial=self.object).aggregate(Avg('star'))
            context['rg'] = Rating.objects.get(profile=self.request.user, serial=self.object).star_id
            context['profiles'] = Profile.objects.get(id=self.request.user.id).id
            context['seasons'] = Season.objects.filter(serial=self.object)
            context['res'] = Serial.objects.get(published=True, id=self.object.id)
            return context
        except:
            return redirect('not_serial')

    def not_serial(request):
        return render(request, 'serials/serial_list.html')


class DetailSeasonView(DetailView):
    model = Season
    template_name = 'serials/season_detail.html'

    def get_object(self):
        """ Избавляет от ошибки FieldError Cannot resolve keyword 'slug' into field"""
        slug = self.kwargs['slug']

    def get_context_data(self, **kwargs):
        """ Возвращает контекст конкретного сезона конкретного сериала"""
        context = super(DetailSeasonView, self).get_context_data(**kwargs)
        a = Serial.objects.get(slug=self.kwargs['slug'])
        context['season'] = Season.objects.get(serial=a, number=self.kwargs['number'])
        return context


class ListFavoriteSerialView(ListView):
    model = FavoriteSerial
    template_name = 'serials/favorite_serial_list.html'
    #paginate_by = 10

    def get_queryset(self):
        """ Показывает любимые сериалы текущего пользователя и которые опубликованы"""
        pub = Serial.objects.filter(published=True)
        return FavoriteSerial.objects.filter(profile=self.request.user, serial__in=pub)


class SerialCreateView(CreateView):
    model = Serial
    template_name = 'serials/serial_new.html'
    fields = ['title', 'description', 'poster', 'year', 'country']
    success_url = '/serials/'


    def form_valid(self,form):
        form.instance.slug = slugify(form.instance.title)
        # form.save()
        # d = Serial.objects.order_by('-id').first()
        # print(d)
        # Rating.objects.create(profile=self.request.user, serial=d)
        return super(SerialCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["starform"] = RatingForm()
        return context


class FavoriteSerialCreateView(CreateView):
    model = FavoriteSerial
    template_name = 'serials/favorite_serial_new.html'
    fields = ['serial']


    def form_valid(self,form):
        """ Привязывает любимый сериал к текущему профилю и обрабатывает исключение, относящееся к UniqueConstraint"""
        try:
            form.instance.profile = self.request.user
            return super(FavoriteSerialCreateView, self).form_valid(form)

        except IntegrityError:
            return redirect('error_serial')
            #return self.form_invalid(form)


def error_serial(request):
    return render(request, 'serials/error_serial.html')


class AddStarRating(View):
    """Добавление рейтинга сериалу"""

    def post(self, request):
        form = RatingForm(request.POST)
        if form.is_valid():
            Rating.objects.update_or_create(
                serial_id=int(request.POST.get("movie")),
                profile_id=int(request.POST.get("profile")),
                defaults={'star_id': int(request.POST.get("star"))}
            )
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)
