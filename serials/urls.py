from django.urls import  path
from django.conf import settings
from django.conf.urls.static import static
from serials.views import ListSerialView, DetailSerialView, ListFavoriteSerialView,\
    FavoriteSerialCreateView, AddStarRating, DetailSeasonView, SerialCreateView

from serials import views


urlpatterns = [
    #path('', views.serials, name='serials'),
    path('', ListSerialView.as_view(), name='serial_list_url'),
    path("add-rating/", views.AddStarRating.as_view(), name='add_rating'),
    path('<str:slug>', DetailSerialView.as_view(), name='serial_detail_url'),
    path('<slug:slug>/<int:number>', DetailSeasonView.as_view(), name='season_detail_url'),
    #path('<str:slug>/<int:number>', views.season_serial, name='season_detail_url'),
    path('myfavorite/', ListFavoriteSerialView.as_view(), name='favorite_serial_list_url'),
    path('myfavorite_add/', FavoriteSerialCreateView.as_view(), name='favorite_serial_new_url'),
    path('serial_add/', SerialCreateView.as_view(), name='serial_new_url'),
    path('error_serial/', views.error_serial, name='error_serial'),


]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)