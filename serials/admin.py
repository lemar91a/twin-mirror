from django.contrib import admin
from serials.models import Serial, FavoriteSerial, RatingStar, Rating, Season


class SerialAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'profile')


class FavoriteSerialAdmin(admin.ModelAdmin):
    list_display = ('id', 'profile', 'serial')

class RatingStarlAdmin(admin.ModelAdmin):
    list_display = ('id', 'value')

class RatingAdmin(admin.ModelAdmin):
    list_display = ('ip', 'star', 'serial')

class SeasonAdmin(admin.ModelAdmin):
    list_display = ('id', 'serial')

admin.site.register(Serial)
admin.site.register(FavoriteSerial)
admin.site.register(RatingStar)
admin.site.register(Rating)
admin.site.register(Season)