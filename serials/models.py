from django.db import models
from profilnet.models import Profile
from django.urls import reverse
#from django.template.defaultfilters import slugify


class Serial(models.Model):
    poster = models.ImageField(upload_to='poster/', blank=True, null=True)
    title = models.CharField("Название", max_length=100)
    description = models.TextField("Описание")
    slug = models.SlugField(max_length=255)
    year = models.PositiveSmallIntegerField("Дата выхода", default=2019, null=True)
    country = models.CharField("Страна", max_length=35, null=True)
    published = models.BooleanField(default=False, blank=True, null=True)

    # def save(self, *args, **kwargs):
    #     self.slug = slugify(self.title)
    #     super(Serial, self).save(*args, **kwargs)


    # def get_absolute_url(self):
    #     if Serial.objects.get(published=True, id=self.object.id):
    #         return reverse('serial_detail_url', kwargs={'slug': self.slug})
    #     else:
    #         return reverse('serial_list_url')

    def get_absolute_url(self):
            return reverse('serial_detail_url', kwargs={'slug': self.slug})


    def __str__(self):
        return self.title

class Season(models.Model):
    serial = models.ForeignKey(Serial, on_delete=models.CASCADE, blank=True, null=True)
    number = models.SmallIntegerField("Номер сезона", default=1)
    description = models.TextField("Краткое описание")

    def __str__(self):
        return f'{self.number} - сезон сериала {self.serial}'


class FavoriteSerial(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, blank=True, null=True)
    serial = models.ForeignKey(Serial, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['profile', 'serial'], name='unique_object'),
        ]
        # эта команда не даст повторно выбирать
        # unique_together = ['serial', 'profile']

    def __str__(self):
        return f'{self.serial} - любимый сериал пользователя {self.profile}'

    def get_absolute_url(self):
        return reverse('favorite_serial_list_url')


class RatingStar(models.Model):
    """Звезда рейтинга"""
    value = models.SmallIntegerField("Значение", default=0)

    def __str__(self):
        return f'{self.value}'

    class Meta:
        verbose_name = "Звезда рейтинга"
        verbose_name_plural = "Звезды рейтинга"
        ordering = ["-value"]


class Rating(models.Model):
    """Рейтинг"""
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, verbose_name="профиль",  blank=True, null=True)
    star = models.ForeignKey(RatingStar, on_delete=models.CASCADE, verbose_name="звезда", default=0)
    serial = models.ForeignKey(Serial, on_delete=models.CASCADE, verbose_name="cериал")

    def __str__(self):
        return f"{self.star} - {self.serial} - {self.profile}"

    class Meta:
        verbose_name = "Рейтинг"
        verbose_name_plural = "Рейтинги"